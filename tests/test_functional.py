import json
import os
from pathlib import Path
import unittest
from datadirtest import DataDirTester, TestDataDir
from keboola.component import CommonInterface

from src.exasol import ExasolClient


class ExasolWriterIntegrationTest(TestDataDir):
    def run_component(self):
        config_dict = self.ci.configuration.config_data
        super().run_component()
        if config_dict["parameters"]["incremental"]:
            super().run_component()
        self.download_result_tables()

    def get_client(self):
        config_dict = self.ci.configuration.config_data

        case_sensitive = True
        if config_dict['parameters']['caseSensitive'] == "Non case sensitive":
            case_sensitive = False

        exasol_client = ExasolClient(config_dict["parameters"]['db']['user'],
                                     config_dict['parameters']['db']['schema'],
                                     config_dict['parameters']['db']['host'],
                                     config_dict['parameters']['db']['port'],
                                     True,
                                     token=config_dict['parameters']['db']['#refresh_token'],
                                     case_sensitive=case_sensitive)
        exasol_client.login()
        return exasol_client

    def download_result_tables(self):
        config_dict = self.ci.configuration.config_data

        exasol_client = self.get_client()

        out_tables_path = self.ci.tables_out_path
        if not os.path.exists(out_tables_path):
            os.makedirs(out_tables_path)

        table_name = config_dict['parameters']['dbName']
        out_table_name = f"{table_name}.csv"
        out_table = os.path.join(out_tables_path, out_table_name)
        exasol_client.download_table(table_name, out_table, order_column=1)

    def _inject_exasol_credentials(self):
        config_dict = self.ci.configuration.config_data
        user = os.environ['EXASOL_CLOUD_USER']
        schema = os.environ['EXASOL_CLOUD_SCHEMA']
        host = os.environ['EXASOL_CLOUD_HOST']
        port = os.environ['EXASOL_CLOUD_PORT']
        refresh_token = os.environ['EXASOL_CLOUD_REFRESH_TOKEN']

        config_dict["parameters"]['db']['user'] = user
        config_dict['parameters']['db']['schema'] = schema
        config_dict['parameters']['db']['host'] = host
        config_dict['parameters']['db']['port'] = port
        config_dict['parameters']['db']['#refresh_token'] = refresh_token

        with open(os.path.join(self.ci.data_folder_path, 'config.json'), 'w+') as cfg_out:
            json.dump(config_dict, cfg_out)

    def setUp(self):
        """
        Executes before test.
        """
        super().setUp()
        source_out_files = os.path.join(self.data_dir, "source", "data", "out", "files")
        expected_out_files = os.path.join(self.data_dir, "expected", "data", "out", "files")
        source_out_tables = os.path.join(self.data_dir, "source", "data", "out", "tables")
        source_in_files = os.path.join(self.data_dir, "source", "data", "in", "files")
        Path(source_out_files).mkdir(parents=True, exist_ok=True)
        Path(source_in_files).mkdir(parents=True, exist_ok=True)
        Path(source_out_tables).mkdir(parents=True, exist_ok=True)
        Path(expected_out_files).mkdir(parents=True, exist_ok=True)
        # just because I'm lazy
        self.ci = CommonInterface(data_folder_path=os.path.join(self.data_dir, "source", "data"))
        # load secrets from environment
        self._inject_exasol_credentials()

    def tearDown(self) -> None:
        """
        Executes after test
        """
        exasol_client = self.get_client()
        tables_in_schema = exasol_client.get_tables_in_schema()
        tables_in_schema = [table_data[1] for table_data in tables_in_schema]
        for table in tables_in_schema:
            exasol_client.drop_table(table)


class TestComponent(unittest.TestCase):

    def test_functional(self):
        functional_tests = DataDirTester(test_data_dir_class=ExasolWriterIntegrationTest)
        functional_tests.run()


if __name__ == "__main__":
    unittest.main()
