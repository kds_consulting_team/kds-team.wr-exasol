# Exasol Writer

EXASOL is a high-performance, in-memory, MPP database specifically designed for analytics.

This component enables you to write data into an exasol schema. If tables exist you can overwrite them, append to them,
or update them. If tables do not exist they can be created

**Table of contents:**

[TOC]

# Authorization

- Username (user) - [REQ]
- Personal Access Token (#refresh_token) - [REQ] "Password" that can be gotten from the 3. step (copy login) in "Connect
  via tools"
- Host (host) - [REQ] Host link eg. xyz.xyz.exasol.com
- Port (port) - [REQ] port of exasol host, default is 8563
- Schema (schema) - [REQ] Name of schema in exasol

# Row config

- Table name (dbName) - [REQ] Name of table in Exasol
- Primary key (primaryKey) - [OPT] Primary key for table in Exasol, only one is allowed
- Columns (items) - [OPT] Dictionary with the following elements:
    - Column name (name) - [OPT] name of column in Keboola
    - Firebolt column name (dbName) - [OPT] Name of column in Exasol table
    - Data type (type) - [OPT] type of data VARCHAR(256), INTEGER, etc.
    - Nullable (nullable) - [OPT] False if not nullable
- Loading Options (loading_options) - [OPT] If set to Incremental update, duplicate rows will be replaced and new rows
  will be inserted. Full load overwrites the destination table each time. Append mode will be append to a table (No
  checks are made for duplicate data)

# Sample configuration

```json
{
  "parameters": {
    "db": {
      "user": "user@mail.com",
      "#refresh_token": "YOUR_PAT_HERE",
      "host": "xxxxx.xxx.exasol.com",
      "port": "8563",
      "schema": "MYSCHEMA"
    },
    "items": [
      {
        "name": "first_name",
        "dbName": "first_name",
        "type": "VARCHAR(256)",
        "nullable": false,
        "size": ""
      },
      {
        "name": "last_name",
        "dbName": "last_name",
        "type": "VARCHAR(256)",
        "nullable": false,
        "size": ""
      },
      {
        "name": "message",
        "dbName": "message",
        "type": "VARCHAR(256)",
        "nullable": false,
        "size": ""
      },
      {
        "name": "pay",
        "dbName": "pay",
        "type": "INTEGER",
        "nullable": false,
        "size": ""
      },
      {
        "name": "extra",
        "dbName": "extra",
        "type": "VARCHAR(256)",
        "nullable": false,
        "size": ""
      }
    ],
    "dbName": "mytable",
    "loading_options": {
      "load_type": "Incremental Update"
    },
    "primaryKey": "first_name"
  }
}


```

Development
-----------

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in the docker-compose
file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone repo_path my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/)