import pyexasol
from random import randint
import logging
import csv
import operator
from pyexasol.exceptions import ExaConnectionDsnError, ExaRequestError, ExaConnectionFailedError, ExaQueryError
from pyexasol.statement import ExaStatement
from typing import Any
from typing import Dict
from typing import List

TABLE_DESC_COLUMN_NAME_POSITION = 3


class ExasolClientError(Exception):
    pass


class ExasolClient:
    def __init__(self, user: str, schema: str, host: str, port: str, debug: bool, password=None,
                 token=None, case_sensitive=True) -> None:
        self.logger = logging.getLogger("exasol_logger")
        if debug:
            self.logger.setLevel(logging.DEBUG)
        self.user = user
        self.token = token
        self.password = password
        self.schema = schema
        self.host = host
        self.port = port
        self.dsn = ":".join([host, port])
        self.client = None
        self.d_qoute = "\""
        self.s_qoute = "\'"
        if not case_sensitive:
            self.d_qoute = ""
            self.s_qoute = ""
        self.case_sensitive = case_sensitive

    def set_case_sensitive(self, case_sensitive):
        if case_sensitive:
            self.d_qoute = "\""
        elif not case_sensitive:
            self.d_qoute = ""

    def login(self) -> None:
        try:
            if self.token:
                self.client = pyexasol.connect(dsn=self.dsn,
                                               user=self.user,
                                               refresh_token=self.token,
                                               protocol_version=3,
                                               schema=self.schema,
                                               encryption=True,
                                               quote_ident=self.case_sensitive)
            elif self.password:
                self.client = pyexasol.connect(dsn=self.dsn,
                                               user=self.user,
                                               password=self.password,
                                               protocol_version=3,
                                               schema=self.schema,
                                               encryption=True,
                                               quote_ident=self.case_sensitive)
            else:
                raise ExasolClientError(
                    "You must specify a Personal Access Token or Username and Password to log into Exasol")
        except ExaConnectionDsnError as connection_exc:
            raise ExasolClientError(connection_exc.message) from connection_exc
        except ExaConnectionFailedError as connection_exc:
            raise ExasolClientError(f"{connection_exc.message}. Please recheck port number and that your "
                                    f"Exasol database/cluster is on") from connection_exc
        except ExaRequestError as auth_error:
            raise ExasolClientError(
                f"{auth_error.message}. Please recheck Personal Access token validity and schema") from auth_error

    def execute_query(self, query: str) -> ExaStatement:
        try:
            return self.client.execute(query)
        except ExaQueryError as query_error:
            raise ExasolClientError(f"{query_error.message}. {query_error.query}")

    def check_if_table_exists(self, table_name: str) -> bool:
        table_exists = False
        if not self.case_sensitive:
            table_name = table_name.upper()
        query = f"""
        SELECT * FROM EXA_ALL_TABLES WHERE TABLE_SCHEMA = '{self.schema}' 
        and TABLE_NAME = '{table_name}' """  # noqa
        self.logger.debug(f"running query {query}")
        results = self.execute_query(query)
        results = results.fetchall()
        if results:
            table_exists = True
        return table_exists

    def get_tables_in_schema(self) -> List:
        query = f"""
        SELECT * FROM EXA_ALL_TABLES WHERE TABLE_SCHEMA = '{self.schema}'
        """
        self.logger.debug(f"running query {query}")
        results = self.execute_query(query)
        results = results.fetchall()
        return results

    def get_table_columns(self, table_name: str) -> List[str]:
        if not self.case_sensitive:
            table_name = table_name.upper()
        query = f"""
        SELECT * FROM EXA_ALL_COLUMNS WHERE COLUMN_SCHEMA = '{self.schema}' 
        AND COLUMN_TABLE =  '{table_name}'
        """  # noqa
        self.logger.debug(f"running query {query}")
        results = self.execute_query(query)
        results = results.fetchall()
        columns = []
        for result in results:
            columns.append(result[TABLE_DESC_COLUMN_NAME_POSITION].strip())
        return columns

    def create_table(self, table_name: str, columns: List[Dict], primary_key: str = "") -> None:
        columns_string = self.get_columns_string(columns, primary_key)
        query = f"""
        CREATE TABLE {self.d_qoute}{table_name}{self.d_qoute} ({columns_string})
        """
        self.logger.debug(f"running query {query}")
        self.execute_query(query)

    def upload_file_to_table(self, file_path: str, table_name: str) -> None:
        logging.info("Uploading data to staging table")
        try:
            self.client.import_from_file(file_path, table_name, import_params={'skip': 1})
            stmt = self.client.last_statement()
            print(f'IMPORTED {stmt.rowcount()} rows in {stmt.execution_time}s')
        except pyexasol.exceptions.ExaQueryError as exc:
            raise ExasolClientError(exc.message)

    def download_table(self, table_name: str, file_name: str, order_column=None):
        self.client.export_to_file(file_name, table_name, export_params={'with_column_names': False})
        if order_column is not None:
            self.order_file(file_name, order_column)

    def order_file(self, file_name, order_column):
        with open(file_name, newline='') as csvfile:
            rdr = csv.reader(csvfile)
            li = sorted(rdr, key=operator.itemgetter(order_column), reverse=True)
        with open(file_name, 'w') as csvout:
            wrtr = csv.writer(csvout)
            wrtr.writerows(li)

    def get_columns_string(self, input_columns: List[Dict], primary_key: str) -> str:
        column_strings = []
        for input_column in input_columns:
            not_null_str = "" if input_column["nullable"] else "NOT NULL"
            pkey_str = "PRIMARY KEY" if input_column["name"] == primary_key else ""
            size_str = f"({input_column['size']})" if input_column["type"] in ["VARCHAR", "CHAR", "DECIMAL"] else ""
            column_str = f"""
                {self.d_qoute}{input_column['name']}{self.d_qoute} {input_column["type"]} {size_str} {not_null_str} {pkey_str}
                """  # noqa
            column_strings.append(column_str)
        return ", ".join(column_strings)

    def swap_tables(self, live_table_name: str, staging_table_name: str) -> None:
        temp_table_name = "_".join([staging_table_name, "swap_table"])

        rename_live_to_temp = f"""
        RENAME TABLE {self.d_qoute}{live_table_name}{self.d_qoute} TO {self.d_qoute}{temp_table_name}{self.d_qoute};
        """
        self.logger.debug(f"running query {rename_live_to_temp}")
        self.execute_query(rename_live_to_temp)

        rename_staging_to_live = f"RENAME TABLE {self.d_qoute}{staging_table_name}{self.d_qoute} " \
                                 f"To {self.d_qoute}{live_table_name}{self.d_qoute}"
        self.logger.debug(f"running query {rename_staging_to_live}")
        self.execute_query(rename_staging_to_live)

        self.drop_table(temp_table_name)

    def drop_table(self, table_name: str) -> None:
        drop_query = f"DROP TABLE {self.d_qoute}{table_name}{self.d_qoute};"
        self.logger.debug(f"running query {drop_query}")
        self.execute_query(drop_query)

    def create_staging_table_like(self, live_table_name: str) -> str:
        staging_table_name = self.generate_staging_table_name(live_table_name)
        query = f"CREATE TABLE {self.d_qoute}{staging_table_name}{self.d_qoute}" \
                f" LIKE {self.d_qoute}{live_table_name}{self.d_qoute};"
        self.logger.debug(f"running query {query}")
        self.execute_query(query)
        return staging_table_name

    @staticmethod
    def random_int_string(n: int) -> str:
        range_start = 10 ** (n - 1)
        range_end = (10 ** n) - 1
        randint(range_start, range_end)
        return str(randint(range_start, range_end))

    def upsert(self, live_table_name: str, staging_table_name: str, primary_key: str,
               columns: List[Dict[str, Any]]) -> None:
        merge_column_string = self.get_merge_column_string(columns, primary_key, "live", "staging")
        insert_values_string = self.get_insert_values_string(columns, "staging")
        merge_query = f"""  MERGE INTO {self.d_qoute}{live_table_name}{self.d_qoute} as live
                        USING {self.d_qoute}{staging_table_name}{self.d_qoute} as staging
                        ON live.{self.d_qoute}{primary_key}{self.d_qoute} = staging.{self.d_qoute}{primary_key}{self.d_qoute}
                        WHEN MATCHED THEN UPDATE SET {merge_column_string}
                        WHEN NOT MATCHED THEN INSERT VALUES ({insert_values_string});
                       """  # noqa
        self.logger.debug(f"running query {merge_query}")
        self.execute_query(merge_query)

    def get_merge_column_string(self, input_columns: List[Dict[str, Any]], primary_key: str, live_table_name: str,
                                stage_table_name: str) -> str:
        column_strings = []
        for input_column in input_columns:
            if input_column["name"] != primary_key:
                col_str = f"{live_table_name}.{self.d_qoute}{input_column['name']}{self.d_qoute} = " \
                          f"{stage_table_name}.{self.d_qoute}{input_column['name']}{self.d_qoute} "
                column_strings.append(col_str)
        return ", ".join(column_strings)

    def get_insert_values_string(self, input_columns: List[Dict[str, Any]], stage_table_name: str) -> str:
        column_strings = []
        for input_column in input_columns:
            col_str = f"{stage_table_name}.{self.d_qoute}{input_column['name']}{self.d_qoute}"
            column_strings.append(col_str)
        return ", ".join(column_strings)

    def insert_from_staging_table(self, table_name: str, staging_table_name: str) -> None:
        query = f"INSERT INTO {self.d_qoute}{table_name}{self.d_qoute} " \
                f"SELECT * FROM {self.d_qoute}{staging_table_name}{self.d_qoute}"
        self.logger.debug(f"running query {query}")
        self.execute_query(query)

    def generate_staging_table_name(self, live_table_name):
        rand_int = self.random_int_string(10)
        return "_".join(["staging", live_table_name, rand_int])
