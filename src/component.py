import logging
from exasol import ExasolClient, ExasolClientError
from typing import List, Any, Dict
from keboola.component.dao import TableDefinition
from keboola.component.base import ComponentBase, UserException
import os

KEY_DATABASE_PARAMS = "db"
KEY_EXASOL_USERNAME = "user"
KEY_EXASOL_TOKEN = '#refresh_token'
KEY_EXASOL_PASSWORD = "#password"
KEY_SCHEMA_NAME = "schema"
KEY_HOST = "host"
KEY_PORT = "port"
KEY_PRIMARY_KEY = "primaryKey"
KEY_DEBUG = "debug"

KEY_CASE_SENSITIVE = "caseSensitive"
KEY_TABLES = "tables"
KEY_DB_TABLE_NAME = "dbName"
KEY_INCREMENTAL = "incremental"
KEY_COLUMNS = "items"

REQUIRED_PARAMETERS = [KEY_DATABASE_PARAMS, KEY_DB_TABLE_NAME]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    def __init__(self) -> None:
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)
        self.exasol_client = None

    def init_client(self) -> ExasolClient:
        try:
            params = self.configuration.parameters
            dubug = params.get(KEY_DEBUG, True)
            db_params = params.get(KEY_DATABASE_PARAMS)
            exasol_user = db_params.get(KEY_EXASOL_USERNAME)
            os.environ["USER"] = exasol_user
            refresh_token = db_params.get(KEY_EXASOL_TOKEN)
            password = db_params.get(KEY_EXASOL_PASSWORD)
            schema = db_params.get(KEY_SCHEMA_NAME)
            host = db_params.get(KEY_HOST)
            port = db_params.get(KEY_PORT)

            params = self.configuration.parameters
            case_sensitive = True
            if params.get(KEY_CASE_SENSITIVE, "Case sensitive") == "Non case sensitive":
                case_sensitive = False

            exasol_client = ExasolClient(exasol_user, schema, host, port, dubug, password=password,
                                         token=refresh_token, case_sensitive=case_sensitive)
            exasol_client.login()
            return exasol_client
        except ExasolClientError as exasol_exc:
            raise UserException(exasol_exc) from exasol_exc

    def test_connection(self):
        self.init_client()

    def run(self) -> None:
        self.exasol_client = self.init_client()

        params = self.configuration.parameters
        input_table = self.get_input_table()
        input_columns = params.get(KEY_COLUMNS)
        table_name = params.get(KEY_DB_TABLE_NAME)
        incremental = params.get(KEY_INCREMENTAL)
        primary_keys = params.get(KEY_PRIMARY_KEY, [])

        self.validate_primary_key(primary_keys, input_columns, incremental)
        primary_key = primary_keys[0] if 0 < len(primary_keys) else None

        self.create_table_if_not_exist(table_name, input_columns, primary_key)

        if incremental:
            self.write_incremental(table_name, input_table, primary_key, input_columns)
        else:
            self.write_full(table_name, input_table, primary_key, input_columns)

    def get_input_table(self) -> TableDefinition:
        input_tables = self.get_input_tables_definitions()
        if len(input_tables) == 0:
            raise UserException("No input table added. Please add an input table")
        elif len(input_tables) > 1:
            raise UserException("Too many input tables added. Please add only one input table")
        return input_tables[0]

    @staticmethod
    def validate_columns(input_columns: List[Dict], existing_table_columns: List[str]) -> None:
        existing_table_columns = [existing_table_column.lower() for existing_table_column in existing_table_columns]
        input_columns = [input_column["name"].lower() for input_column in input_columns]
        error_messages = []
        if len(input_columns) != len(existing_table_columns):
            error_messages.append(f"The amount of columns in the input table ({len(input_columns)}) does not match the "
                                  f"amount of columns in the existing table in Exasol ({len(existing_table_columns)})")
        for input_column in input_columns:
            if input_column not in existing_table_columns:
                error_messages.append(
                    f"Column {input_column} not in existing table in Exasol")
        for existing_table_column in existing_table_columns:
            if existing_table_column not in input_columns:
                error_messages.append(
                    f"Column {existing_table_column} is missing in the input table")
        if error_messages:
            raise UserException(". ".join(error_messages))

    @staticmethod
    def validate_primary_key(primary_keys: List, input_columns: List[Dict], incremental: bool) -> None:
        if len(primary_keys) > 1:
            raise UserException("Only one primary key can be set")
        primary_key = primary_keys[0] if 0 < len(primary_keys) else None
        if incremental and not primary_key:
            raise UserException("To use incremental, please set a primary key")
        primary_key_in_columns = False
        for input_column in input_columns:
            if primary_key == input_column["dbName"]:
                primary_key_in_columns = True
        if primary_key and not primary_key_in_columns and incremental:
            raise UserException("Primary key is not in the columns of the table")

    def create_table_if_not_exist(self, table_name, input_columns, primary_key):
        logging.info(f"Checking if table {table_name} exists")
        table_exists = self._check_if_table_exists(table_name)

        if not table_exists:
            logging.info(f"Table {table_name} does not exists, creating it")
            self._create_table(table_name, input_columns, primary_key)

    def write_incremental(self, table_name: str, input_table: TableDefinition, primary_key: str,
                          input_columns: List[Dict]) -> None:

        logging.info("Uploading using incremental update")
        existing_table_columns = self._get_exasol_columns(table_name)
        self.validate_columns(input_columns, existing_table_columns)
        staging_table_name = self.exasol_client.create_staging_table_like(table_name)
        self._upload_file_to_table(input_table.full_path, staging_table_name)
        self._upsert(table_name, staging_table_name, primary_key, input_columns)
        self._drop_table(staging_table_name)

    def write_full(self, table_name: str, input_table: TableDefinition, primary_key: str,
                   input_columns: List[Dict]) -> None:
        logging.info("Uploading using full load")
        staging_table_name = self.exasol_client.generate_staging_table_name(table_name)
        self._create_table(staging_table_name, input_columns, primary_key)
        self._upload_file_to_table(input_table.full_path, staging_table_name)
        self._swap_tables(table_name, staging_table_name)

    def _drop_table(self, table_name: str) -> None:
        try:
            self.exasol_client.drop_table(table_name)
        except ExasolClientError as exasol_error:
            raise UserException(exasol_error) from exasol_error

    def _upsert(self, table_name: str, staging_table_name: str, primary_key: str,
                input_columns: List[Dict[str, Any]]) -> None:
        try:
            self.exasol_client.upsert(table_name, staging_table_name, primary_key, input_columns)
        except ExasolClientError as exasol_error:
            raise UserException(exasol_error) from exasol_error

    def _upload_file_to_table(self, input_table_path: str, staging_table_name: str) -> None:
        try:
            self.exasol_client.upload_file_to_table(input_table_path, staging_table_name)
        except ExasolClientError as exasol_error:
            raise UserException(exasol_error) from exasol_error

    def _swap_tables(self, table_name: str, staging_table_name: str) -> None:
        try:
            self.exasol_client.swap_tables(table_name, staging_table_name)
        except ExasolClientError as exasol_error:
            raise UserException(exasol_error) from exasol_error

    def _check_if_table_exists(self, table_name: str) -> bool:
        try:
            return self.exasol_client.check_if_table_exists(table_name)
        except ExasolClientError as exasol_error:
            raise UserException(exasol_error) from exasol_error

    def _create_table(self, table_name, input_columns, primary_key):
        try:
            self.exasol_client.create_table(table_name, input_columns, primary_key)
        except ExasolClientError as exasol_error:
            raise UserException(exasol_error) from exasol_error

    def _get_exasol_columns(self, table_name: str) -> List[str]:
        try:
            return self.exasol_client.get_table_columns(table_name)
        except ExasolClientError as exasol_error:
            raise UserException(exasol_error) from exasol_error


if __name__ == "__main__":
    try:
        comp = Component()
        # comp.test_connection()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
