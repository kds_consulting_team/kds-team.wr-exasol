This component enables you to write data into an exasol schema. If tables exist you can overwrite them, append to them,
or update them. If tables do not exist they can be created